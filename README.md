##Deploy MariaDB Work Instructions

ansible-playbook -i inventory deploy_mariadb.yaml --limit=mariadb01 --extra-vars "db_root_password=<dbpassword>"

##Deploy HAProxy Work Instructions

ansible-playbook -i inventory deploy_haproxy.yaml --limit=haproxy01
